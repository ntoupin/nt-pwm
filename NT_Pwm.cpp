/*
 * @file NT_Pwm.cpp
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Pwm function definitions.
 */

#include "Arduino.h"
#include "NT_Pwm.h"

Pwm_t::Pwm_t()
{

}

void Pwm_t::Configure(int Startup_Sp, int Deadband_Sp, int Minimum_Sp, int Maximum_Sp, int Minimum_Op, int Maximum_Op)
{
	// Copy to internal parameters
	_Startup_Sp = Startup_Sp;
	_Deadband_Sp = Deadband_Sp;
	_Minimum_Sp = Minimum_Sp;
	_Maximum_Sp = Maximum_Sp;
	_Minimum_Op = Minimum_Op;
	_Maximum_Op = Maximum_Op;
}

void Pwm_t::Attach(int Pin)
{
	// Copy to internal parameters
	_Pin = Pin;

	// Arduino function to set pin as output
	pinMode(_Pin, OUTPUT);
}

void Pwm_t::Init()
{
	Enable = TRUE;
	Value_Sp = 0;
	Value_Sp_Last = 0;
}

void Pwm_t::Deinit()
{
	Enable = FALSE;
}

void Pwm_t::Set(int Setpoint)
{
	if (Enable == TRUE)
	{
		// Floor and roof Sp of pwm limits
		if (Setpoint < _Minimum_Sp)
		{
			Value_Sp = _Minimum_Sp;
		}
		else if (Setpoint > _Maximum_Sp)
		{
			Value_Sp = _Maximum_Sp;
		}
		else
		{
			Value_Sp = Setpoint;
		}

		// Check if difference between new and old value is more than the defined deadband
		int Difference = abs(Value_Sp - Value_Sp_Last);
		
		// Set new pwm Sp if difference is more than defined deadband
		if (Difference >= _Deadband_Sp)
		{
			// Set pwm to current setpoint value because difference is more than the deadband
			Value_Op = map(Value_Sp, _Minimum_Sp, _Maximum_Sp, _Minimum_Op, _Maximum_Op);
			Value_Sp_Last = Value_Sp;			
		}
		else
		{
			// Set pwm to last setpoint value because difference is not more than the deadband
			Value_Op = map(Value_Sp_Last, _Minimum_Sp, _Maximum_Sp, _Minimum_Op, _Maximum_Op);
		}
	}
	else
	{
		// Set pwm to startup value
		Value_Op = map(_Startup_Sp, _Minimum_Sp, _Maximum_Sp, _Minimum_Op, _Maximum_Op);
	}

	// Write pwm
	analogWrite(_Pin, Value_Op);

	//Serial.println(Value_Op);
}