/*
 * @file NT_Pwm.h
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Pwm function declarations.
 */

#ifndef NT_Pwm_h

#include "Arduino.h"

#define TRUE 1
#define FALSE 0

#define NT_Pwm_h

class Pwm_t
{
public:
	Pwm_t();
	bool Enable = FALSE;
	int Value_Sp = 0;	
	int Value_Sp_Last = 0;
	int Value_Op = 0;
	void Configure(int Startup_Sp, int Deadband_Sp, int Minimum_Sp, int Maximum_Sp, int Minimum_Op, int Maximum_Op);
	void Attach(int Pin);
	void Init();
	void Deinit();
	void Set(int Setpoint);

private:
	int _Pin;
	int _Startup_Sp = 0;
	int _Deadband_Sp = 0;
	int _Minimum_Sp = 0;
	int _Maximum_Sp = 0;
	int _Minimum_Op = 0;
	int _Maximum_Op = 0;
};

#endif