/*
 * @file Pwm.ino
 * @author Nicolas Toupin
 * @date 2019-11-24
 * @brief Simple example of pwm utilisation.
 */

#include <NT_Pwm.h>

Pwm_t Pwm1;

void setup()
{
  Pwm1.Configure(0, 0, 0, 255, 0, 255);
  Pwm1.Attach(9);
  Pwm1.Init();
}

void loop()
{
  for (int i = Pwm1.Minimum_Sp; i < Pwm1.Maximum_Sp; i += 20)
  {
    Pwm1.Set(i);
    delay(10);
  }

  for (int i = Pwm1.Maximum_Sp; i > Pwm1.Minimum_Sp; i -= 20)
  {
    Pwm1.Set(i);
    delay(10);
  }
}
